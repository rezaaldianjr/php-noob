<?php
require 'functions.php';

//ambil data di url
$id = $_GET["id"];

//query data mahasiswa berdasarkan id
$mhs = query("SELECT * FROM mahasiswa WHERE id =$id")[0];




if (isset($_POST["submit"]) ){
   
    //cek data berhasil diubah
    if (ubah($_POST) > 0){
        echo "
        <script> 
        alert('Data berhasil diubah');
        document.location.href='index.php';
        </script>
        ";
    } else {
        echo "
        <script> 
        alert('Data Gagal DiUbah');
        </script>";
    }


 };
?>



<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Tambah Data Mahasiswa</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <h1> Ubah Data Mahasiswa</h1>

    <form action=""  method="post">
        <input type="hidden" name="id" value="<?= $mhs["id"]; ?>">
        <ul>
            <li>
                <label for="nrp">NRP :</label>
            <input type="text" name="nrp" id="nrp" required 
            value="<?php echo $mhs["nrp"];?>">
            </li>
            <li>
                <label for="nama">Nama :</label>
            <input type="text" name="nama" id="nama" required
            value="<?php echo $mhs["nama"];?>">
            </li>
            <li>
                <label for="email">Email :</label>
            <input type="text" name="email" id="email"
            value="<?php echo $mhs["email"];?>">
            </li>
            <li>
                <label for="jurusan">Jurusan :</label>
            <input type="text" name="jurusan" id="jurusan"
            value="<?php echo $mhs["jurusan"];?>">
            </li>
            <li>
                <label for="gambar">Gambar :</label>
            <input type="text" name="gambar" id="jurusan"
            value="<?php echo $mhs["gambar"];?>">
            </li>
            <li>
                <button type="submit" name="submit">Ubah</button>
            </li>
</ul>
</body>
</html>