<?php

//variabel superglobals tipenya array assosiative
// $_GET, $_POST, $_REQUEST, $_SESSION
// $_COOKIE, $_SERVER, $_ENV

// var_dump($_POST);
// var_dump($_SERVER);
// echo $_SERVER["SERVER_NAME"];
//masukin data ke array asosiativ , $_GET["nama"] = "Reza Aldian"; 
// $_GET["nim"] = "1411155";
//atau bisa di URL, misal localhost/phptugas.php?nama=Reza&nim=14111155
// var_dump($_GET);

$mahasiswa = [
    [
    "nim" => "14111155", 
    "nama" => "Reza Aldian",
    "email" => "reza@gmail.com",
    "jurusan" => "Teknik Informatika",
    "gambar" => "reza.jpg"
    ],
    [
        "nama" => "Carl Johnson", 
        "nim" => "9933155",
        "email" => "Udin@gmail.com",
        "jurusan" => "Teknik Industri",
        "gambar" => "reza2.jpeg"
        ]
];


?>
<!DOCTYPE html>
<html>
<head>
    <title>Daftar MAhasiswa</title>
</head>
<body>
    <h1>Daftar Mahasiswa</h1>
    <ul>
<?php foreach ($mahasiswa as $mhs) : ?>
<li>
   <a href="latihan2.php?nama=<?php echo $mhs["nama"];?>&nim=<?= $mhs["nim"]; ?>
   &email=<?= $mhs["email"]; ?>&jurusan=<?= $mhs["jurusan"]; ?>
   &gambar=<?= $mhs["gambar"]; ?>"> <?php echo $mhs["nama"];?></a>
</li>
<?php endforeach; ?>
</ul>
</body>
</html>