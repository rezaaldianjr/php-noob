<?php
//cek data biar tidak hit direct dari URL tanpa bawa data
if(!isset($_GET["nama"]) ||
   !isset($_GET["nim"])  ||
   !isset($_GET["email"]) ||
   !isset($_GET["jurusan"]) ||
   !isset($_GET["nim"])){
  //redirect kembali
  header("Location: latihan.php");
  exit;  
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Mahasiswa</title>
</head>
<body>
 <ul>
        <li><img src="img/<?= $_GET["gambar"];?>"></li>
        <li><?php echo $_GET["nama"]; ?></li>
        <li><?= $_GET["nim"];?></li>
        <li><?= $_GET["email"]; ?></li>
        <li><?= $_GET["jurusan"]; ?></li>

</ul>
<a href="latihan.php">Kembali ke daftar mahasiswa</a>

</body>
</html>