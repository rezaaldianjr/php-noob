<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Latihan POST</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
 <?php if(isset ($_POST["submit"])):?>
    <h1>Halo, Selamat Datang <?=$_POST["nama"]; ?> </h1>
    <?php endif;?>
    
    <!-- pada tag form harus ada atribut action dan method  -->
    <!-- jika action kosong, defaultnya dikirim ke page ini , 
    method kosong maka nilai defaultnya adalah get -->
    <form action="" method="post">
    masukan nama : <input type="text" name="nama">
    <br>
    <button type="submit" name="submit">KIRIM</button>


</form>
    
</body>
</html>