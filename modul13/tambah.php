<?php
require 'functions.php';


if (isset($_POST["submit"]) ){
   
    //cek data berhasil ditambah
    if (tambah($_POST) > 0){
        echo "
        <script> 
        alert('Data berhasil ditambahkan');
        document.location.href='index.php';
        </script>
        ";
    } else {
        echo "
        <script> 
        alert('Data Gagal Ditambah');
        </script>";
    }


 };
?>



<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Tambah Data Mahasiswa</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <h1> Tambah Data Mahasiswa</h1>

    <form action=""  method="post">
        <ul>
            <li>
                <label for="nrp">NRP :</label>
            <input type="text" name="nrp" id="nrp" required>
            </li>
            <li>
                <label for="nama">Nama :</label>
            <input type="text" name="nama" id="nama">
            </li>
            <li>
                <label for="email">Email :</label>
            <input type="text" name="email" id="email">
            </li>
            <li>
                <label for="jurusan">Jurusan :</label>
            <input type="text" name="jurusan" id="jurusan">
            </li>
            <li>
                <label for="gambar">Gambar :</label>
            <input type="text" name="gambar" id="jurusan">
            </li>
            <li>
                <button type="submit" name="submit">Simpan</button>
            </li>
</ul>
</body>
</html>