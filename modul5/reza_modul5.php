<?php
//Pengulangan
//for
//while
// do.. while
//foreach : khusus array

// for( $i = 0 ; $i<5; $i++) {
//     echo "halo dunia <br>";
// }


// $i=0; /*inisiasi */
// while ( $i<5){   /*terminasi (kapan stop) */
// echo "Halo Dunia<br>"; /*apa yang dilakukan */
// $i++; /*increment/decrement nya */
// }

// $i=10;
// do {
//     echo "Helo dunia <br>";
//  $i++;   

// } while( $i <5);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
        <title>Latihan</title>
  <style> 
   .warna-baris { 
    background-color: silver;
   }
    </style>
</head>
<body>
<table border="3" cellpadding="10" cellspacing="2">
 <!-- <?php 
for ($i=1; $i<=3; $i++){
    echo "<tr>";
    for($j=1; $j <=5; $j++){
      echo "<td>$i,$j</td>"; 
    }
  
}
?>  -->
<!-- penggunaan templating php kurawal diganti tanda ":"
dan akhir kurawal diganti "endfor,endif -->

  <?php for ($i=1; $i<=5; $i++) : ?>
    <?php if ($i % 2 == 1) : ?>
  <tr class="warna-baris">
    <?php else : ?>
    <tr>
  <?php endif ?>
  <?php for($j=1; $j <=5; $j++) : ?>
    <td><?php echo "$i,$j"; ?> </td> 
  <?php endfor; ?>
  </tr>
  <?php endfor; ?>

</table>
</body>
</html>
