<?php
// $mahasiswa= [
//     ["Reza Aldian", "14111155", "reza@gmail.com",
// "Teknik Informatika"],
// ["Carl", "17888122", "CJ@gmail.com",
// "Teknik Industri"]
// ];

//array Asosiatif (dengan string sebagai key /
//  bukan index atau array numeric),kalo kebalik juga ga masalah
$mahasiswa = [
    [
    "nim" => "14111155", 
    "nama" => "reza",
    "email" => "reza@gmail.com",
    "jurusan" => "Teknik Informatika",
    "gambar" => "reza.jpg"
    ],
    [
        "nama" => "Udin", 
        "nim" => "9933155",
        "email" => "Udin@gmail.com",
        "jurusan" => "Teknik Industri",
        "gambar" => "reza2.jpeg"
        ]
];

// echo $mahasiswa [1]["tugas"][1];
?>
 <!DOCTYPE html>
<html>
<head>
    <title>Daftar MAhasiswa</title>
</head>
<body>
    <h1>Daftar Mahasiswa</h1>
<?php foreach ($mahasiswa as $mhs) : ?>

    <ul>
        <li> 
            <img src="img/<?= $mhs["gambar"];?>">
        </li>
        <li>Nama: <?php echo $mhs["nama"]; ?></li>
        <li>NIM : <?php echo $mhs["nim"]; ?></li>
        <li>Email:  <?php echo $mhs["email"]; ?></li>
        <li>Jurusan: <?php echo $mhs["jurusan"]; ?></li>
</ul>
<?php endforeach; ?>
</body>
</html>
