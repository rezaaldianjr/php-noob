<?php
//array multidimensi (array inside array)
$mahasiswa = [
    ["reza","14111155"," Teknik 
              Informatika", "rezaaldianjr@gmail.com"],
    ["udin","1675634155"," Teknik 
              Industri", "udin@gmail.com"]
];


?>

<html>
    <head>
        <title>Daftar Mahasiswa </title>
</head>
<body>
    <h1>Daftar Mahasiswa</h1>

<!-- //untuk array yg biasa menampilkan seperti ini -->
    <!-- <ul>
   <?php foreach ($mahasiswa as $mhs):   ?>
    <li><?php echo $mhs; ?></li>

    <?php endforeach; ?> -->

<!-- untuk array multidimensi  -->
 <?php foreach($mahasiswa as $mhs): ?>
    <ul>
        <li>nama :<?php echo $mhs[0];?></li>
        <li>nim :<?php echo $mhs[1];?></li>
        <li>Jurusan :<?php echo $mhs[2];?></li>
        <li>email :<?php echo $mhs[3];?></li>
 </ul>
        
   <?php endforeach;?>


</body>
</html>