<?php
//pengulangan pada array
//memakai for / foreach 
$angka = [3,4,6,7,8,1,44,87];
?>

<html>
    <head>
        <title>Latihan array </title>
        <style>
        .kotak {
            width: 50px;
            height: 50px;
            background-color: salmon;
            text-align : center;
            line-height: 50px;
            margin: 3px;
            float: left;
        }
         .clear {clear:both;}
</style>
</head>
<body>
<?php for($i=0; $i < count($angka); $i++) {       ?>

   <div class="kotak"><?php echo $angka[$i]; ?> </div>
<?php } ?>

<div class="clear"></div>
<?php foreach( $angka as $a ) {   ?>
  <div class="kotak"><?php echo $a; ?> </div>
<?php } ?>

<div class="clear"></div>

<?php foreach($angka as $a) :  ?>
    <div class="kotak"> <?php echo $a; ?></div>
    <?php endforeach ?>



</body>
</html>