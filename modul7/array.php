<?php
//array-> pasangan key dan value
//key = index dimulai dari 0
//cara membuat array
//elemen boleh pakai tipe data yg berbeda

//cara lama 
$hari= array("senin","selasa","rabu");
//cara baru
$bulan = ["januari","februari", "maret"];

//menampilkan array
//bisa pakai var_dump()  / print_r()

// var_dump($hari);
// echo "<br>";
// print_r ($bulan);
// echo "<br>";


//menampilkan satu elemen pada array
// echo $bulan[1];


//menambahkan data ke array
var_dump($hari);
$hari[]="kamis";
$hari[]="jumat";
echo "<br>";
var_dump($hari);




?>



