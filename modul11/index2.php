<?php
//koneksi ke database
$conn = mysqli_connect("localhost","root","","php_noob");

//ambil data dari tabel mahasiswa/query
$result=mysqli_query($conn,"SELECT * FROM mahasiswa");
//untuk cek eror nya
// if (!$result){
//     echo mysqli_error($conn);
// }

//ambil data dari object result mahasiswa (fetch)
//mysqli_fetch_row() //mengembalikan array numerik
//mysqli_fetch_assoc() //mengembalikan array assosiative var_dump($mhs["nama"]);
//mysqli_fetch_array() //mengembalikan numerik atau asosiatif, fleksibel, tapi
                       //tapi data yg disajikan double,
//mysqli_fetch_object() //mengembalikan objek , panggil var_dump($mhs->email); 

//di looping pakai while
// while ($mhs = mysqli_fetch_assoc($result)){ 
// var_dump($mhs);
// }


?>

<html>
<head> 
    <title>Halaman Admin</title>
</head>

<body>
<h1>Daftar Mahasiswa</h1>
<table border="1" cellpadding="10" cellspacing="0">
<tr>
    <th>No.</th>
    <th>Aksi</th>
    <th>Gambar</th>
    <th>NRP</th>
    <th>Nama</th>
    <th>Email</th>
    <th>Jurusan</th>

</tr>
<?php $i = 1;?> 
<!-- agar nomor di tabel tidak ngikut ke database -->
<?php while($row = mysqli_fetch_assoc($result)) :?>
<tr>
    <td><?= $i;  ?></td>
    <td> 
        <a href="">Ubah</a> |
        <a href="">Hapus</a>
    </td>
    <td><img src="img/<?= $row["gambar"];?>" width="70"></td>
    <td><?= $row["nrp"]; ?></td>
    <td><?= $row["nama"]; ?></td>
    <td><?= $row["email"]; ?></td>
    <td><?= $row["jurusan"];?></td>
</tr>
<?php $i++?>
<?php endwhile; ?>
</table>

</body>
</html>