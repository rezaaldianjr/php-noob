<?php
//Pertemuan 2 -PHP dasar
// Sintaks Php

//Standar output
//echo, print
//print_r
// var_dump
// echo "Reza aldian ";
// print "Reza Aldian ";
// print_r("Rezaaaaa ");
// var_dump("aku Reza");

//Penulisan Sintaks PHP
//1. PHP didalam HTML
//2. HTML didalam PHP

//Variabel dan Tipe data
//variabel
//tidak boleh diawali dengan angka, tapi boleh mengandung angka
// $nama = "Reza Aldian";
// echo "HAlo, nama saya $nama";

//operator
//aritmatika + - * / %
// $x=10;
// $y=20;
// echo $x * $y;

//penggabung STring/ concatentaion /concat
// $nama_depan= "Reza";
// $nama_belakang="Aldian";
// echo $nama_depan . " ". $nama_belakang;

//Assignment
// = , +=, -=, *=, /=, %=, .=
// $x=1;
// $x +=5;
// echo $x;

//Perbandingan 
// >, <, <=, >=, ==, !=

//identitas ,cek tipedata
// ===, !==
// var_dump(1 === "1");

//logika
//&&, ||, !
 $x = 30;
 var_dump($x < 20 || $x % 2 ==0);




?>

